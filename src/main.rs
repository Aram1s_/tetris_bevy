use std::time::Duration;

use bevy::{
    diagnostic::{FrameTimeDiagnosticsPlugin, LogDiagnosticsPlugin},
    prelude::*,
    sprite::collide_aabb::collide,
    window::{PresentMode, WindowMode},
};

const BACKGROUND_COLOR: Color = Color::rgb(0.9, 0.9, 0.9);
const TETROMINO_OFFSET: Vec2 = Vec2::new(HALF_CELL_SIZE, CELL_SIZE * 9.);
const PREVIEW_OFFSET: Vec2 = Vec2::new(CELL_SIZE * 10., 0.0);

const TETROMINO_DELAY: u64 = 500;

const TETROMINO_I: [Vec2; 4] = [
    Vec2::new(0. * CELL_SIZE, 1. * CELL_SIZE),
    Vec2::new(0. * CELL_SIZE, -1. * CELL_SIZE),
    Vec2::new(0. * CELL_SIZE, 0. * CELL_SIZE),
    Vec2::new(0. * CELL_SIZE, 2. * CELL_SIZE),
];
const TETROMINO_O: [Vec2; 4] = [
    Vec2::new(0. * CELL_SIZE, 0. * CELL_SIZE),
    Vec2::new(1. * CELL_SIZE, 0. * CELL_SIZE),
    Vec2::new(0. * CELL_SIZE, 1. * CELL_SIZE),
    Vec2::new(1. * CELL_SIZE, 1. * CELL_SIZE),
];
const TETROMINO_T: [Vec2; 4] = [
    Vec2::new(0. * CELL_SIZE, 1. * CELL_SIZE),
    Vec2::new(0. * CELL_SIZE, 0. * CELL_SIZE),
    Vec2::new(1. * CELL_SIZE, 1. * CELL_SIZE),
    Vec2::new(-1. * CELL_SIZE, 1. * CELL_SIZE),
];
const TETROMINO_J: [Vec2; 4] = [
    Vec2::new(0. * CELL_SIZE, 1. * CELL_SIZE),
    Vec2::new(0. * CELL_SIZE, 0. * CELL_SIZE),
    Vec2::new(-1. * CELL_SIZE, 0. * CELL_SIZE),
    Vec2::new(0. * CELL_SIZE, 2. * CELL_SIZE),
];
const TETROMINO_L: [Vec2; 4] = [
    Vec2::new(0. * CELL_SIZE, 1. * CELL_SIZE),
    Vec2::new(0. * CELL_SIZE, 0. * CELL_SIZE),
    Vec2::new(1. * CELL_SIZE, 0. * CELL_SIZE),
    Vec2::new(0. * CELL_SIZE, 2. * CELL_SIZE),
];
const TETROMINO_S: [Vec2; 4] = [
    Vec2::new(0. * CELL_SIZE, 0. * CELL_SIZE),
    Vec2::new(-1. * CELL_SIZE, 0. * CELL_SIZE),
    Vec2::new(0. * CELL_SIZE, 1. * CELL_SIZE),
    Vec2::new(1. * CELL_SIZE, 1. * CELL_SIZE),
];
const TETROMINO_Z: [Vec2; 4] = [
    Vec2::new(0. * CELL_SIZE, 0. * CELL_SIZE),
    Vec2::new(1. * CELL_SIZE, 0. * CELL_SIZE),
    Vec2::new(0. * CELL_SIZE, 1. * CELL_SIZE),
    Vec2::new(-1. * CELL_SIZE, 1. * CELL_SIZE),
];

const CELL_SIZE: f32 = 40.0;
const HALF_CELL_SIZE: f32 = CELL_SIZE / 2.;

const WALL_COLOR: Color = Color::rgb(0.6, 0.6, 0.6);
const WALL_THICKNESS: f32 = 10.0;

const LEFT_WALL: f32 = -5. * CELL_SIZE - WALL_THICKNESS / 2.;
const RIGHT_WALL: f32 = 5. * CELL_SIZE + WALL_THICKNESS / 2.;
const BOTTOM_WALL: f32 = -10.5 * CELL_SIZE - WALL_THICKNESS / 2.;
const TOP_WALL: f32 = 10.5 * CELL_SIZE + WALL_THICKNESS / 2.;

const SCOREBOARD_FONT_SIZE: f32 = 80.0;
const SCOREBOARD_TEXT_PADDING: Val = Val::Px(5.0);
const TEXT_COLOR: Color = Color::rgb(0.5, 0.5, 1.0);
const SCORE_COLOR: Color = Color::rgb(1.0, 0.5, 0.5);

fn main() {
    App::new()
        .add_plugins((
            DefaultPlugins.set(WindowPlugin {
                primary_window: Some(Window {
                    title: "Tetris bevy".into(),
                    resolution: (CELL_SIZE * 12., CELL_SIZE * 22.).into(),
                    present_mode: PresentMode::Immediate,
                    mode: WindowMode::Fullscreen,
                    fit_canvas_to_parent: false,
                    prevent_default_event_handling: false,
                    ..default()
                }),
                ..default()
            }),
            (
                LogDiagnosticsPlugin::default(),
                FrameTimeDiagnosticsPlugin::default(),
            ),
        ))
        .add_state::<GameState>()
        .init_resource::<ActiveTetromino>()
        .insert_resource(ClearColor(BACKGROUND_COLOR))
        .insert_resource(Scoreboard { score: 0, lines: 0 })
        .insert_resource(InputTimer(Timer::from_seconds(
            1. / 120.,
            TimerMode::Repeating,
        )))
        .add_event::<CollisionEvent>()
        .add_systems(Startup, camera_setup)
        .add_systems(OnEnter(GameState::Playing), setup)
        .add_systems(
            Update,
            (
                spawn_new_tetromino,
                input,
                can_move.before(move_tetromino),
                move_tetromino.after(can_move),
                clear_lines,
                can_rotate.before(rotate_tetromino),
                rotate_tetromino.after(can_rotate),
                calculate_slam.before(slam),
                slam.after(calculate_slam),
                update_scoreboard,
            ).run_if(in_state(GameState::Playing)),
        )
        .add_systems(OnExit(GameState::Playing), teardown)
        .add_systems(Update, gameover_keyboard.run_if(in_state(GameState::GameOver)))
        .add_systems(OnExit(GameState::GameOver), teardown)
        .add_systems(OnEnter(GameState::GameOver), display_score)
        .add_systems(OnEnter(GameState::Quit), exit)
        .add_systems(Update, bevy::window::close_on_esc)
        .run();
}

#[derive(Resource)]
struct Scoreboard {
    score: usize,
    lines: usize,
}

#[derive(PartialEq, Default)]
struct MoveableDirections {
    left: bool,
    right: bool,
    down: bool,
}

#[derive(Resource)]
struct InputTimer(Timer);

#[derive(Component)]
struct PlaceDelay {
    timer: Timer,
}

#[derive(Resource, Default)]
struct ActiveTetromino {
    direction: Direction,
    rotation: Rotation,
    kind: Tetromino,
    speed: f32,
    center: Vec2,
    can_rotate: bool,
    can_move: MoveableDirections,
    can_slam: bool,
    slam_pos: Vec<f32>,
    can_clear: bool,
    next_tetromino: Tetromino,
}

#[derive(Component)]
struct Cell;

#[derive(Component)]
struct Preview;

#[derive(Component)]
struct Block;

#[derive(Component)]
struct Collider;

#[derive(Default, Event)]
struct CollisionEvent;

#[derive(Bundle)]
struct WallBundle {
    sprite_bundle: SpriteBundle,
    collider: Collider,
}

#[derive(Bundle)]
struct TetrominoBundle {
    sprite_bundle: SpriteBundle,
}

#[derive(Clone, Copy, Default, Eq, PartialEq, Debug, Hash, States)]
enum GameState {
    #[default]
    Playing,
    GameOver,
    Quit,
}

enum WallLocation {
    Left,
    Right,
    Bottom,
    Top,
}

#[derive(Default, PartialEq)]
enum Direction {
    #[default]
    Neutral,
    Down,
    Left,
    Right,
    SlamDown,
}

#[derive(Default)]
enum Rotation {
    #[default]
    Neutral,
    Left,
    Right,
    Flip,
}

#[derive(Copy, Clone, Default, PartialEq)]
enum Tetromino {
    #[default]
    I,
    O,
    T,
    J,
    L,
    S,
    Z,
}

impl Tetromino {
    fn gen_bundle(tetromino: Tetromino, offset: Vec2) -> Vec<TetrominoBundle> {
        let mut result = Vec::new();
        for i in 0..4 {
            result.push(TetrominoBundle {
                sprite_bundle: SpriteBundle {
                    transform: Transform {
                        translation: tetromino.position().map(|t| (t + offset).extend(1.))[i],
                        scale: Vec2::new(CELL_SIZE, CELL_SIZE).extend(0.),
                        ..default()
                    },
                    sprite: Sprite {
                        color: tetromino.color(),
                        ..default()
                    },
                    ..default()
                },
            });
        }
        result
    }
    fn position(&self) -> [Vec2; 4] {
        match self {
            Tetromino::I => TETROMINO_I,
            Tetromino::O => TETROMINO_O,
            Tetromino::T => TETROMINO_T,
            Tetromino::J => TETROMINO_J,
            Tetromino::L => TETROMINO_L,
            Tetromino::S => TETROMINO_S,
            Tetromino::Z => TETROMINO_Z,
        }
    }
    fn color(&self) -> Color {
        match self {
            Tetromino::I => Color::CYAN,
            Tetromino::O => Color::YELLOW,
            Tetromino::T => Color::PURPLE,
            Tetromino::J => Color::BLUE,
            Tetromino::L => Color::ORANGE,
            Tetromino::S => Color::GREEN,
            Tetromino::Z => Color::RED,
        }
    }
    fn rand_tetromino() -> Tetromino {
        use rand::prelude::*;
        let mut rng = rand::thread_rng();
        let rn = rng.gen_range(0..7);
        match rn {
            0 => Tetromino::I,
            1 => Tetromino::O,
            2 => Tetromino::T,
            3 => Tetromino::J,
            4 => Tetromino::L,
            5 => Tetromino::S,
            6 => Tetromino::Z,
            _ => panic!("tried to spawn non existing tetromino"),
        }
    }
    fn spawn(mut commands: Commands, tetromino: Tetromino, next: Tetromino) {
        let offset = match tetromino {
            Tetromino::I => TETROMINO_OFFSET - CELL_SIZE,
            Tetromino::J => TETROMINO_OFFSET - CELL_SIZE,
            Tetromino::L => TETROMINO_OFFSET - CELL_SIZE,
            _ => TETROMINO_OFFSET,

        };
        for cell in Tetromino::gen_bundle(tetromino, offset) {
            commands.spawn((cell, Cell));
        }
        for cell in Tetromino::gen_bundle(next, PREVIEW_OFFSET) {
            commands.spawn((cell, Preview));
        }
    }
    fn spawn_block(commands: &mut Commands, tetromino: Tetromino, thing: Vec<Vec3>) {
        for t in thing {
            let mut tr = t.clone();
            tr.y = (t.y / CELL_SIZE).round() * CELL_SIZE;
            tr.x = (t.x / HALF_CELL_SIZE).round() * HALF_CELL_SIZE;
            commands.spawn((
                TetrominoBundle {
                    sprite_bundle: SpriteBundle {
                        transform: Transform {
                            translation: tr,
                            scale: Vec2::new(CELL_SIZE, CELL_SIZE).extend(0.),
                            ..default()
                        },
                        sprite: Sprite {
                            color: tetromino.color(),
                            ..default()
                        },
                        ..default()
                    },
                },
                Block,
                Collider,
            ));
        }
    }
}

impl WallLocation {
    fn position(&self) -> Vec2 {
        match self {
            WallLocation::Left => Vec2::new(LEFT_WALL, 0.),
            WallLocation::Right => Vec2::new(RIGHT_WALL, 0.),
            WallLocation::Bottom => Vec2::new(0., BOTTOM_WALL),
            WallLocation::Top => Vec2::new(0., TOP_WALL),
        }
    }

    fn size(&self) -> Vec2 {
        let arena_height = TOP_WALL - BOTTOM_WALL;
        let arena_width = RIGHT_WALL - LEFT_WALL;
        assert!(arena_height > 0.0);
        assert!(arena_width > 0.0);

        match self {
            WallLocation::Left | WallLocation::Right => {
                Vec2::new(WALL_THICKNESS, arena_height + WALL_THICKNESS)
            }
            WallLocation::Bottom | WallLocation::Top => {
                Vec2::new(arena_width + WALL_THICKNESS, WALL_THICKNESS)
            }
        }
    }
}

impl WallBundle {
    fn new(location: WallLocation) -> WallBundle {
        WallBundle {
            sprite_bundle: SpriteBundle {
                transform: Transform {
                    translation: location.position().extend(0.0),
                    scale: location.size().extend(1.0),
                    ..default()
                },
                sprite: Sprite {
                    color: WALL_COLOR,
                    ..default()
                },
                ..default()
            },
            collider: Collider,
        }
    }
}

fn camera_setup(mut commands: Commands) {
    commands.spawn(Camera2dBundle::default());
}

fn setup(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut tetromino: ResMut<ActiveTetromino>,
) {
    commands.spawn(WallBundle::new(WallLocation::Left));
    commands.spawn(WallBundle::new(WallLocation::Right));
    commands.spawn(WallBundle::new(WallLocation::Bottom));
    commands.spawn(WallBundle::new(WallLocation::Top));

    commands.spawn(
        TextBundle::from_sections([
            TextSection::new(
                "Score: ",
                TextStyle {
                    font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                    font_size: SCOREBOARD_FONT_SIZE,
                    color: TEXT_COLOR,
                },
            ),
            TextSection::from_style(TextStyle {
                font: asset_server.load("fonts/FiraMono-Medium.ttf"),
                font_size: SCOREBOARD_FONT_SIZE,
                color: SCORE_COLOR,
            }),
            TextSection::new(
                "\nLines : ",
                TextStyle {
                    font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                    font_size: SCOREBOARD_FONT_SIZE,
                    color: TEXT_COLOR,
                },
            ),
            TextSection::from_style(TextStyle {
                font: asset_server.load("fonts/FiraMono-Medium.ttf"),
                font_size: SCOREBOARD_FONT_SIZE,
                color: SCORE_COLOR,
            }),
        ])
        .with_style(Style {
            position_type: PositionType::Absolute,
            top: SCOREBOARD_TEXT_PADDING,
            left: SCOREBOARD_TEXT_PADDING,
            ..default()
        }),
    );

    tetromino.kind = Tetromino::rand_tetromino();
    tetromino.next_tetromino = Tetromino::rand_tetromino();
    Tetromino::spawn(commands, tetromino.kind, tetromino.next_tetromino);
    tetromino.center = tetromino.kind.clone().position()[0] + TETROMINO_OFFSET;
    tetromino.speed = 1.;
}

fn input(
    keyboard_input: Res<Input<KeyCode>>,
    mut tetromino: ResMut<ActiveTetromino>,
    mut next_state: ResMut<NextState<GameState>>,
) {
    if keyboard_input.just_pressed(KeyCode::A) {
        tetromino.direction = Direction::Left;
    } else if keyboard_input.just_pressed(KeyCode::D) {
        tetromino.direction = Direction::Right;
    } else if keyboard_input.pressed(KeyCode::S) {
        tetromino.direction = Direction::Down;
    } else if keyboard_input.just_pressed(KeyCode::J) {
        tetromino.rotation = Rotation::Left;
    } else if keyboard_input.just_pressed(KeyCode::L) {
        tetromino.rotation = Rotation::Right;
    } else if keyboard_input.just_pressed(KeyCode::K) {
        tetromino.rotation = Rotation::Flip;
    } else if keyboard_input.just_pressed(KeyCode::Space) {
        tetromino.direction = Direction::SlamDown;
    } else if keyboard_input.just_pressed(KeyCode::Q) {
        next_state.set(GameState::Quit);
        info!("Entered GameState::Quit");
    } else if keyboard_input.just_pressed(KeyCode::Plus) {
        println!(
            "Moveable directions: \nLeft: {}\nRight: {}\nDown: {}",
            tetromino.can_move.left, tetromino.can_move.right, tetromino.can_move.down
        );
    } else {
        tetromino.rotation = Rotation::Neutral;
        tetromino.direction = Direction::Neutral;
    }
}
fn move_tetromino(
    mut cell_query: Query<&mut Transform, With<Cell>>,
    mut tetromino: ResMut<ActiveTetromino>,
    time: Res<Time>,
    mut timer: ResMut<InputTimer>,
) {
    let mut x = 0.;
    // only add to the y value of the tetromino to avoid falling speed being fps reliant.
    if timer.0.tick(time.delta()).just_finished() && tetromino.can_move.down {
        let mut y = tetromino.speed;
        if tetromino.direction == Direction::Down {
            y *= 10.;
        }
        for mut cell in cell_query.iter_mut() {
            cell.translation.y -= y;
        }
        tetromino.center.y -= y;
    } else if !tetromino.can_move.down {
        for mut cell in cell_query.iter_mut() {
            cell.translation.y = (cell.translation.y / HALF_CELL_SIZE).round() * HALF_CELL_SIZE;
        }
    }

    match tetromino.direction {
        Direction::Left => {
            if tetromino.can_move.left {
                x -= CELL_SIZE
            }
        }
        Direction::Right => {
            if tetromino.can_move.right {
                x += CELL_SIZE
            }
        }
        Direction::SlamDown => tetromino.can_slam = true,
        _ => (),
    }
    tetromino.direction = Direction::Neutral;
    for mut cell in cell_query.iter_mut() {
        cell.translation.x += x;
        cell.translation.x = (cell.translation.x / HALF_CELL_SIZE).round() * HALF_CELL_SIZE;
    }
    tetromino.center.x += x;
    tetromino.center.x = (tetromino.center.x / HALF_CELL_SIZE).round() * HALF_CELL_SIZE;
}

fn can_move(
    mut tetromino: ResMut<ActiveTetromino>,
    cell_query: Query<&Transform, With<Cell>>,
    collider_query: Query<&Transform, With<Collider>>,
    mut commands: Commands,
    delay_query: Query<&PlaceDelay>,
) {
    let mut delay = false;
    for _ in delay_query.iter() {
        delay = true;
    }
    let mut can_move = MoveableDirections {
        left: true,
        right: true,
        down: true,
    };
    let mut offsets = [
        Vec3::new(-CELL_SIZE, 0., 0.),
        Vec3::new(CELL_SIZE, 0., 0.),
        Vec3::new(0., -1., 0.),
    ];
    if tetromino.direction == Direction::Down {
        offsets[2].y *= 10.;
    }
    for cell_transform in cell_query.iter() {
        for (i, offset) in offsets.iter().enumerate() {
            for collider_transform in collider_query.iter() {
                let collision = collide(
                    cell_transform.translation + *offset,
                    cell_transform.scale.truncate(),
                    collider_transform.translation,
                    collider_transform.scale.truncate(),
                );

                if collision.is_some() {
                    match i {
                        0 => can_move.left = false,
                        1 => can_move.right = false,
                        2 => {
                            can_move.down = false;
                            if !delay {
                                commands.spawn(PlaceDelay {
                                    timer: Timer::new(
                                        Duration::from_millis(TETROMINO_DELAY),
                                        TimerMode::Once,
                                    ),
                                });
                            }
                        }
                        _ => panic!("unexpected direction"),
                    }
                }
            }
        }
    }
    tetromino.can_move = can_move;
}

fn can_rotate(
    mut tetromino: ResMut<ActiveTetromino>,
    cell_query: Query<&Transform, With<Cell>>,
    block_query: Query<&Transform, With<Block>>,
) {
    if tetromino.kind == Tetromino::O {
        tetromino.can_rotate = false;
        return;
    }
    let mut r = Vec::new();
    let cells = cell_query.iter();
    let center = tetromino.center;

    for cell in cells {
        let x = center.x - cell.translation.x;
        let y = center.y - cell.translation.y;

        let origin = Vec2::new(center.x, center.y);
        let new_x = -(cell.translation.x - origin.x) + origin.x;
        let new_y = -(cell.translation.y - origin.y) + origin.y;

        match tetromino.rotation {
            Rotation::Left => r.push(Vec2::new(center.x + y, center.y - x)),
            Rotation::Right => r.push(Vec2::new(center.x - y, center.y + x)),
            Rotation::Flip => {
                if tetromino.kind == Tetromino::I
                    || tetromino.kind == Tetromino::S
                    || tetromino.kind == Tetromino::Z
                {
                    tetromino.can_rotate = false;
                    return;
                }
                r.push(Vec2::new(new_x, new_y));
            }
            Rotation::Neutral => {
                tetromino.can_rotate = false;
                return;
            }
        }
    }
    for v in r.clone() {
        if v.x > RIGHT_WALL || v.x < LEFT_WALL || v.y < -10. * CELL_SIZE {
            tetromino.can_rotate = false;
            return;
        }
        for block_transform in block_query.iter() {
            if collide(
                v.extend(1.),
                Vec2::new(1., 1.),
                block_transform.translation,
                block_transform.scale.truncate(),
            )
            .is_some()
            {
                tetromino.can_rotate = false;
                return;
            }
        }
    }
    tetromino.can_rotate = true;
}

fn rotate_tetromino(
    tetromino: Res<ActiveTetromino>,
    mut cell_query: Query<&mut Transform, With<Cell>>,
) {
    if !tetromino.can_rotate {
        return;
    }
    let cells = cell_query.iter_mut();
    for mut cell in cells {
        let center = tetromino.center;
        let x = center.x - cell.translation.x;
        let y = center.y - cell.translation.y;

        let origin = Vec2::new(center.x, center.y);
        let new_x = -(cell.translation.x - origin.x) + origin.x;
        let new_y = -(cell.translation.y - origin.y) + origin.y;

        match tetromino.rotation {
            Rotation::Left => cell.translation = Vec3::new(center.x + y, center.y - x, 1.),
            Rotation::Right => cell.translation = Vec3::new(center.x - y, center.y + x, 1.),
            Rotation::Flip => cell.translation = Vec3::new(new_x, new_y, 1.),
            Rotation::Neutral => (),
        }
    }
}

fn spawn_new_tetromino(
    mut commands: Commands,
    tetromino_query: Query<(Entity, &Transform), With<Cell>>,
    block_query: Query<&Transform, With<Block>>,
    preview_query: Query<Entity, With<Preview>>,
    mut tetromino: ResMut<ActiveTetromino>,
    time: Res<Time>,
    mut delay_query: Query<(Entity, &mut PlaceDelay), With<PlaceDelay>>,
    mut next_state: ResMut<NextState<GameState>>,
) {
    if let Some((delay_entity, mut timer)) = delay_query.iter_mut().next() {
        timer.timer.tick(time.delta());

        if timer.timer.finished() && !tetromino.can_move.down
            || (!tetromino.can_rotate
                && tetromino.can_move
                    == MoveableDirections {
                        left: false,
                        right: false,
                        down: false,
                    })
        {
            let mut result = Vec::new();
            for (_, t) in tetromino_query.iter() {
                result.push(t.translation);
            }
            for (e, _) in tetromino_query.iter() {
                commands.entity(e).despawn();
            }
            for e in preview_query.iter() {
                commands.entity(e).despawn();
            }
            Tetromino::spawn_block(&mut commands, tetromino.kind, result);
            commands.entity(delay_entity).despawn();
        }
    }
    if tetromino_query.iter().len() == 0 {
        tetromino.kind = tetromino.next_tetromino;
        tetromino.next_tetromino = Tetromino::rand_tetromino();
        Tetromino::spawn(commands, tetromino.kind, tetromino.next_tetromino);
        tetromino.center = tetromino.kind.position()[0] + TETROMINO_OFFSET;
        tetromino.can_clear = true;
    }
    for block_transform in block_query.iter() {
        for (_, cell_transform) in tetromino_query.iter() {
            let collision = collide(
                cell_transform.translation,
                cell_transform.scale.truncate(),
                block_transform.translation,
                block_transform.scale.truncate(),
            );
            if collision.is_some() {
                next_state.set(GameState::GameOver);
                info!("Entered GameState::GameOver");
                return;
            }
        }
    }
}

fn clear_lines(
    mut block_query: Query<(Entity, &mut Transform), With<Block>>,
    mut scoreboard: ResMut<Scoreboard>,
    mut commands: Commands,
    mut tetromino: ResMut<ActiveTetromino>,
) {
    if !tetromino.can_clear {
        return;
    }
    tetromino.can_clear = false;

    let mut line_count = 0;
    let mut lines = Vec::new();
    for i in -10..10 {
        let mut count = 0;
        for (_, block) in block_query.iter() {
            if block.translation.y == i as f32 * CELL_SIZE {
                count += 1;
            }
        }
        if count == 10 {
            line_count += 1;
            lines.push(i as f32 * CELL_SIZE);
        }
    }
    match line_count {
        0 => (),
        1 => scoreboard.score += 25,
        2 => scoreboard.score += 100,
        3 => scoreboard.score += 300,
        4 => scoreboard.score += 1200,
        _ => panic!("\n*\nMore than 4 lines at the same time?\n*\n"),
    }
    scoreboard.lines += line_count;
    assert!(lines.len() == line_count);
    for l in lines.iter().rev() {
        for (e, block) in block_query.iter() {
            if block.translation.y == *l {
                commands.entity(e).despawn();
            }
        }
        for (_, mut block) in block_query.iter_mut() {
            if block.translation.y > *l {
                block.translation.y -= CELL_SIZE;
            }
        }
    }
}

fn update_scoreboard(scoreboard: Res<Scoreboard>, mut query: Query<&mut Text>) {
    let mut text = query.single_mut();
    text.sections[1].value = scoreboard.score.to_string();
    text.sections[3].value = scoreboard.lines.to_string();
}

fn calculate_slam(
    cell_query: Query<&Transform, With<Cell>>,
    collider_query: Query<&Transform, With<Collider>>,
    mut tetromino: ResMut<ActiveTetromino>,
) {
    if !tetromino.can_slam {
        return;
    }
    let mut offset = Vec3::new(0., 0., 0.);
    loop {
        for collider_transform in collider_query.iter() {
            for transform in cell_query.iter() {
                let collision = collide(
                    transform.translation - offset,
                    transform.scale.truncate(),
                    collider_transform.translation,
                    collider_transform.scale.truncate(),
                );
                if collision.is_some() {
                    while collide(
                        transform.translation - offset,
                        transform.scale.truncate(),
                        collider_transform.translation,
                        collider_transform.scale.truncate(),
                    )
                    .is_some()
                    {
                        offset.y -= 1.;
                    }
                    let mut vec = Vec::new();
                    for t in cell_query.iter() {
                        vec.push(t.translation.y - offset.y);
                    }
                    tetromino.slam_pos = vec;
                    return;
                }
            }
        }
        offset.y += CELL_SIZE;
    }
}

fn slam(mut tetromino: ResMut<ActiveTetromino>, mut cell_query: Query<&mut Transform, With<Cell>>) {
    if !tetromino.can_slam {
        return;
    }
    let mut iter = tetromino.slam_pos.iter();
    for mut t in cell_query.iter_mut() {
        t.translation.y = *iter.next().unwrap();
    }
    tetromino.can_slam = false;
}

fn teardown(mut commands: Commands, entities: Query<Entity, (Without<Window>, Without<Camera>)>) {
    for entity in &entities {
        commands.entity(entity).despawn();
    }
}

fn exit(mut commands: Commands, entities: Query<Entity>) {
    for entity in &entities {
        commands.entity(entity).despawn();
    }
}

fn display_score(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    scoreboard: Res<Scoreboard>,
) {
    commands
        .spawn(NodeBundle {
            style: Style {
                width: Val::Percent(100.),
                height: Val::Percent(100.),
                align_items: AlignItems::Center,
                justify_content: JustifyContent::Center,
                ..default()
            },
            ..default()
        })
        .with_children(|parent| {
            parent.spawn(TextBundle::from_sections([
                TextSection::new(
                    "Lines: ",
                    TextStyle {
                        font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                        font_size: SCOREBOARD_FONT_SIZE,
                        color: TEXT_COLOR,
                    },
                ),
                TextSection::new(
                    scoreboard.lines.to_string(),
                    TextStyle {
                        font: asset_server.load("fonts/FiraMono-Medium.ttf"),
                        font_size: SCOREBOARD_FONT_SIZE,
                        color: SCORE_COLOR,
                    },
                ),
                TextSection::new(
                    "\nScore: ",
                    TextStyle {
                        font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                        font_size: SCOREBOARD_FONT_SIZE,
                        color: TEXT_COLOR,
                    },
                ),
                TextSection::new(
                    scoreboard.score.to_string(),
                    TextStyle {
                        font: asset_server.load("fonts/FiraMono-Medium.ttf"),
                        font_size: SCOREBOARD_FONT_SIZE,
                        color: SCORE_COLOR,
                    },
                ),
                TextSection::new(
                    "\nPress R to retry\nPress Q to quit",
                    TextStyle {
                        font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                        font_size: SCOREBOARD_FONT_SIZE,
                        color: TEXT_COLOR,
                    },
                ),
            ]));
        });
}

fn gameover_keyboard(
    mut next_state: ResMut<NextState<GameState>>,
    keyboard_input: Res<Input<KeyCode>>,
) {
    if keyboard_input.just_pressed(KeyCode::R) {
        next_state.set(GameState::Playing);
    } else if keyboard_input.just_pressed(KeyCode::Q) {
        next_state.set(GameState::Quit);
    }
}
